INSERT INTO Ciudad(Nombre) VALUES ('Madrid');
INSERT INTO Ciudad(Nombre) VALUES ('Barcelona');
INSERT INTO Temperatura VALUES ('2020-02-10', 1, 17.1);
INSERT INTO Temperatura VALUES ('2020-02-11', 2, 15.32);
INSERT INTO Humedad VALUES ('2020-02-11', 1, 0.6);
INSERT INTO Humedad VALUES ('2020-02-11', 2, 0.3);
INSERT INTO Informe (Dia, Codigo_Ciudad) VALUES ('2020-02-12', 1);
INSERT INTO Informe (Dia, Codigo_Ciudad) VALUES ('2020-02-12', 2);
