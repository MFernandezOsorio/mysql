/*DUDAS:
*/


DROP DATABASE IF EXISTS Ejer1Refuerzo;

CREATE DATABASE Ejer1Refuerzo;
USE Ejer1Refuerzo;

CREATE TABLE CLIENTES (
    DNI CHAR (10),
    Nombre VARCHAR (15),
    Apellidos VARCHAR (25),
    Telefono CHAR (13),
    Email VARCHAR (30) UNIQUE,

    PRIMARY KEY (DNI),
);

CREATE TABLE TIENDAS (
    Nombre VARCHAR (20),
    Provincia VARCHAR (20),
    Localidad VARCHAR (20),
    Dirección VARCHAR (30),
    Telefono CHAR (13),
    DiaApertura ENUM ('Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo'),
    DiaCierre ENUM ('Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo'),
    HoraApertura TINYINT UNSIGNED,
    HoraCierre TINYINT UNSIGNED,

    CONSTRAINT PK_TIENDAS PRIMARY KEY (Nombre)
);

CREATE TABLE OPERADORAS (
    Nombre VARCHAR (20),
    ColorLogo VARCHAR (15),
    PorcentajeCobertura TINYINT UNSIGNED COMMENT 'Unidades en %',
    FrecuenciaGSM SMALLINT UNSIGNED,
    PaginaWeb VARCHAR (30),

    CONSTRAINT PK_OPERADORAS PRIMARY KEY (Nombre)
);

CREATE TABLE TARIFAS (
    Nombre VARCHAR (20),
    Nombre_OPERADORAS VARCHAR (20),
    TamañoDatos ENUM, --FALTAN DATOS!!
    TipoDatos ENUM
    MinutosGratis INT (10),
    SMSGratis ENUM ('Si', 'No'),

    CONSTRAINT PK_TARIFAS PRIMARY KEY (Nombre),
    CONSTRAINT FK_OPERADORAS FOREIGN KEY (Nombre_OPERADORAS) REFERENCES OPERADORAS (Nombre)
);

CREATE TABLE MOVILES (
    Marca VARCHAR (20),
    Modelo VARCHAR (20),
    Descripcion VARCHAR (30),
    SO ENUM ('Android', 'iOS'),
    RAM TINYINT UNSIGNED,
    PulgadasPantalla DECIMAL (3,2) UNSIGNED COMMENT 'Unidades en pulgadas',
    CamaraMpx DECIMAL (2,2) UNSIGNED,

    CONSTRAINT PK_MOVILES PRIMARY KEY (Marca, Modelo)
);





















