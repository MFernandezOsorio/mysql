
#########################TRABAJO DE MARIO FERNÁNDEZ Y ROBERTO FLORES##############################################################


#CONSULTAS

DROP DATABASE IF EXISTS tenerife;
CREATE DATABASE tenerife;

USE tenerife;#Usar la bbdd.

#CREAR TABLAS.

CREATE TABLE ED_CULTURA (clase TINYINT, municipio CHAR(5), nombre VARCHAR(60), sigla CHAR(3), direccion CHAR(70), numero INT(3), referencia VARCHAR(65), cp CHAR(5), web VARCHAR(110), email CHAR(70), telefono CHAR(9), fax CHAR(9));

#LOAD DATA INFILE '/home/roberto/eyd.csv' INTO TABLE ED_CULTURA FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' IGNORE 1 LINES;
#La importación de los .csv se pone como comentario por si implementas las tablas, ya que tiene mi ruta donde se encuentra el archivo.#

CREATE TABLE COMERCIO (clase TINYINT, municipio CHAR(5), nombre VARCHAR(60), sigla CHAR(3), direccion CHAR(70), numero INT(3), referencia VARCHAR(65), cp CHAR(5), web VARCHAR(110), email CHAR(70), telefono CHAR(9), fax CHAR(9));

#LOAD DATA INFILE '/home/roberto/c.csv' INTO TABLE COMERCIO FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' IGNORE 1 LINES;
#La importación de los .csv se pone como comentario por si implementas las tablas, ya que tiene mi ruta donde se encuentra el archivo.#

CREATE TABLE ALIMENTACION (clase TINYINT, municipio CHAR(5), nombre VARCHAR(60), sigla CHAR(3), direccion CHAR(70), numero INT(3), referencia VARCHAR(65), cp CHAR(5), web VARCHAR(110), email CHAR(70), telefono CHAR(9), fax CHAR(9));

#LOAD DATA INFILE '/home/roberto/a.csv' INTO TABLE ALIMENTACION FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' IGNORE 1 LINES;
#La importación de los .csv se pone como comentario por si implementas las tablas, ya que tiene mi ruta donde se encuentra el archivo.#

CREATE TABLE INDUSTRIA (clase TINYINT, municipio CHAR(5), nombre VARCHAR(60), sigla CHAR(3), direccion CHAR(70), numero INT(3), referencia VARCHAR(65), cp CHAR(5), web VARCHAR(110), email CHAR(70), telefono CHAR(9), fax CHAR(9)); 

#LOAD DATA INFILE '/home/roberto/1.csv' INTO TABLE INDUSTRIA FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' IGNORE 1 LINES;
#La importación de los .csv se pone como comentario por si implementas las tablas, ya que tiene mi ruta donde se encuentra el archivo.#

#Nos muestra los comercios de moda y electrónica con los comercios de alimentación que están en el mismo municipio.
SELECT COMERCIO.nombre AS 'Comerciante', COMERCIO.telefono AS 'CONTACTO', COMERCIO.municipio, ALIMENTACION.municipio, ALIMENTACION.nombre AS 'Comerciado', ALIMENTACION.telefono AS 'CONTACTO ALIMENT.' FROM COMERCIO JOIN ALIMENTACION ON COMERCIO.municipio = 38035 && ALIMENTACION.municipio=38035;

#Encontrar los lugares de cultura y ocio que estén en la zona postal 38400.
SELECT count(ED_CULTURA.nombre) AS "Cantidad", ED_CULTURA.nombre, CONCAT(ED_CULTURA.telefono, "-", ED_CULTURA.fax) AS 'Telf-Fax' FROM ED_CULTURA JOIN ALIMENTACION ON ALIMENTACION.cp = 38400 && ED_CULTURA.fax!="" GROUP BY ED_CULTURA.nombre ORDER BY ED_CULTURA.nombre DESC;

#Dime la dirección web y email, el nombre de las industrias que tengan distinto teléfono y los puestos de alimentación que tengan email registrado.
SELECT CONCAT(ALIMENTACION.email, " / ", ALIMENTACION.web) AS 'informacion', INDUSTRIA.nombre AS 'Nombre de Industria' FROM ALIMENTACION, INDUSTRIA WHERE ALIMENTACION.email!="" && INDUSTRIA.telefono!=ALIMENTACION.telefono GROUP BY INDUSTRIA.nombre;

#Quiero hacer una vista para tener mas accesible la cantidad de empresas de alimentacion que hay en todos los municipios.
CREATE VIEW vista1 AS SELECT count(municipio) AS 'Numero de municipios', CONCAT('Nom: ', nombre, '/ Telf:', telefono) AS 'Nombre y telefono' FROM ALIMENTACION GROUP BY nombre LIMIT 200;

#Los comercios e industrias que tengan el mismo código postal, agrupados por codigo postal y que tengan un id menos que 5000.
SELECT CONCAT(COMERCIO.nombre, ' - ', COMERCIO.cp) AS COMERCIO, CONCAT(INDUSTRIA.nombre, INDUSTRIA.cp) AS 'INDUSTRIA' FROM COMERCIO, INDUSTRIA WHERE COMERCIO.cp = INDUSTRIA.cp GROUP BY COMERCIO.cp HAVING MAX(COMERCIO.id) < 5000;




#USERS
/*
admin -> todos privilegios.
desarrollador -> select, create, delete, drop, insert, update.
atencionCliente -> puede select, update, nombre, email y telefono.
cliente -> puede select municipio, nombre, direccion, codigo postal, web, email, tlf, fax.
*/ 

DROP USER IF EXISTS 'admin'@'localhost';
CREATE USER 'admin'@'localhost' IDENTIFIED BY 'qwertyU123.'; #Todos los permisos.

DROP USER IF EXISTS 'desarrollador'@'localhost';
CREATE USER 'desarrollador'@'localhost' IDENTIFIED BY 'Desarrollador.1'; #Permiso de SELECT, CREATE, DELETE, DROP, INSERT y UPDATE.

DROP USER IF EXISTS 'atencionCliente'@'localhost';
CREATE USER 'atencionCliente'@'localhost' IDENTIFIED BY 'Atencioncliente.1'; #Permiso de SELECT, UPDATE en nombre, email, telefono y fax.

DROP USER IF EXISTS 'cliente'@'localhost';
CREATE USER 'cliente'@'localhost' IDENTIFIED BY 'Cliente.1'; #Permiso de SELECT municipio, nombre, direccion, codigo postal, web, email, tlf, fax.





#PRIVILEGES

GRANT ALL ON tenerife.* TO 'admin'@'localhost' WITH GRANT OPTION; #puede otorgar a otros users cualquiera de los privilegios.

GRANT SELECT, CREATE, DELETE, DROP, INSERT, UPDATE ON tenerife.* TO 'desarrollador'@'localhost';

GRANT SELECT, UPDATE (nombre, email, telefono, fax) ON tenerife.COMERCIO TO 'atencionCliente'@'localhost';
GRANT SELECT, UPDATE ON tenerife.ED_CULTURA TO 'atencionCliente'@'localhost';
GRANT SELECT, UPDATE ON tenerife.ALIMENTACION TO 'atencionCliente'@'localhost';
GRANT SELECT, UPDATE ON tenerife.INDUSTRIA TO 'atencionCliente'@'localhost';#Se dan los permisos tabla por tabla, no se puede en un único paso.

GRANT SELECT (municipio, nombre, direccion, cp, web, email, telefono, fax) ON tenerife.COMERCIO TO 'cliente'@'localhost';
GRANT SELECT (municipio, nombre, direccion, cp, web, email, telefono, fax) ON tenerife.ED_CULTURA TO 'cliente'@'localhost';
GRANT SELECT (municipio, nombre, direccion, cp, web, email, telefono, fax) ON tenerife.ALIMENTACION TO 'cliente'@'localhost';
GRANT SELECT (municipio, nombre, direccion, cp, web, email, telefono, fax) ON tenerife.INDUSTRIA TO 'cliente'@'localhost';

flush privileges;





#CREATE PROCEDURE

DROP PROCEDURE IF EXISTS numeroMENOR;
DELIMITER //

CREATE PROCEDURE numeroMENOR (IN num INT(3)) #Se crea el procedimiento llamado numeroMENOR.

	BEGIN
		SELECT CONCAT(COMERCIO.nombre, ' - ', COMERCIO.telefono) AS 'COMERCIO', CONCAT(INDUSTRIA.nombre, ' - ',INDUSTRIA.telefono) AS 'INDUSTRIA' FROM COMERCIO, INDUSTRIA WHERE COMERCIO.numero>num && INDUSTRIA.numero>num && COMERCIO.telefono != "" && INDUSTRIA.telefono != "";
	END // #Pedimos que nos muestre los comercios e industrias menores que el numero insertado y que tengan registrados teléfonos.

DELIMITER ; 






#FUNCION 

DROP FUNCTION IF EXISTS idCOM;

DELIMITER //

CREATE FUNCTION idCOM(idc INT(3)) #Se crea la funcion idCOM.
	RETURNS CHAR(40) #En esta funcion no es necesario añadir el parámetro DETERMINISTIC.

	BEGIN
		DECLARE result CHAR(40);
		SET result = (SELECT nombre FROM COMERCIO WHERE id = idc); #Muestre el nombre de comercio que tenga el id igual que el introducido.
		IF idc>=6789 THEN SET result = 'Los ids son inferiores a 6789'; #si se pasa del número mostrará ese mensaje de error.
		END IF; #FIN del IF
		RETURN result; #Devuelve el resultado del select.

	END //
#Termina la funcion.
DELIMITER ;

#FINAL