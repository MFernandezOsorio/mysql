DROP DATABASE Starwars;
CREATE DATABASE IF NOT EXISTS Starwars;

USE Starwars;

CREATE TABLE Personajes(
    Codigo INTEGER PRIMARY KEY,
    Nombre VARCHAR(30),
    Raza VARCHAR(20),
    Grado VARCHAR(20),
    CodigoActor INTEGER REFERENCES Actores(Codigo)
);
CREATE TABLE Actores (
    Codigo INTEGER PRIMARY KEY,
    Nombre VARCHAR(40),
    Fecha DATE,
    Nacionalidad VARCHAR(20)
);

ALTER TABLE Personajes ADD FOREIGN KEY(CodigoActor) REFERENCES Actores(Codigo) ON UPDATE CASCADE;

INSERT INTO Actores VALUES (1,'Ewan McGregor', '1980-01-01', 'Escocés');
INSERT INTO Actores VALUES (2,'Natalie Portman', '1980-01-01', 'Isralí');
INSERT INTO Actores VALUES (3,'Samuel L.Jackson','1970-01-01','Americana');

INSERT INTO Personajes VALUES (1, 'Obi Wan Ke No Ve', 'Humano', 'Maestro',1);
INSERT INTO Personajes VALUES (2, 'Princesa Amigdala', 'Naboo', 'Ninguno',2);
INSERT INTO Personajes VALUES (3, 'Maese Windows', 'Hunamuno', 'Maestro',3);

UPDATE Actores SET Codigo=5 WHERE Codigo=2;

SET FOREIGN_KEY_CHECKS=0;
DELETE FROM Actores WHERE Codigo=5;
SET FOREIGN_KEY_CHECKS=1;
