DROP DATABASE IF EXISTS EjerRepaso6;
CREATE DATABASE EjerRepaso6;
USE EjerRepaso6;

CREATE TABLE ZOO (
    nombre VARCHAR (20),
    ciudad VARCHAR (20),
    pais VARCHAR (20),
    tamaño ENUM ('CATEGORIA A', 'CATEGORIA B'),
    presupuesto INT UNSIGNED,

    CONSTRAINT ZOO_PK PRIMARY KEY (nombre)
);

CREATE TABLE ESPECIE (
    nomcientífico VARCHAR (20),
    nomvulgar VARCHAR NOT NULL(20),
    familia VARCHAR (15),
    peligro ENUM ('SI', 'NO'),

    CONSTRAINT ESPECIE_PK PRIMARY KEY (nomcientifico)
);

CREATE TABLE ANIMAL (
    ID INT UNSIGNED,
    nomzoo VARCHAR (20),
    nomespecie VARCHAR (20),
    sexo ENUM ('MASCULINO', 'FEMENINO'),
    añonacim YEAR,
    pais VARCHAR (15),
    continente VARCHAR (15),

    CONSTRAINT ANIMAL_FK PRIMARY KEY (ID),

    CONSTRAINT ANIMAL_ZOO_FK FOREIGN KEY (nomzoo)
                         REFERENCES ZOO (nombre)
                         ON DELETE CASCADE
                         ON UPDATE CASCADE,

    CONSTRAINT ANIMAL_ESPECIE_FK FOREIGN KEY (nomespecie)
                                 REFERENCES ESPECIE (nomcientifico)
                                 ON DELETE CASCADE
                                 ON UPDATE CASCADE
);


